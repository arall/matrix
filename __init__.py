import leds
import random

from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from st3m.ui.colours import WHITE, BLACK
from ctx import Context


class Matrix(Application):
    def __init__(self, context):
        super().__init__(context)

        # Settings
        self.drop_steps = 25
        self.charset = 'abcdefghijklmnopqrstuvwxyz0123456789'
        self.length = 15
        self.initial_y = [-200, -100]
        self.space_between_chars = 15
        self.space_between_drops = 20
        self.y_range = 80
        self.font = 3
        self.font_size = 20

        # Rain definition
        self.rain = []
        for x in range(-100, 120, self.space_between_drops):
            self.defineDrop(x)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        for i in range(40):
            leds.set_rgb(i, 0, random.randint(50, 255), 0)
        leds.update()

    def draw(self, ctx: Context) -> None:
        # Background
        ctx.rgb(*BLACK).rectangle(-120, -120, 240, 240).fill()

        # Text settings
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = self.font_size
        ctx.font = ctx.get_font_name(self.font)

        # Rain
        for drop_key, drop in enumerate(self.rain):

            # Update the drop position
            drop['y'] += self.drop_steps
            self.rain[drop_key] = drop

            # Draw the drop
            for char_key, char in enumerate(drop['chars']):

                # Color
                if char_key == 0:
                    ctx.rgb(*WHITE)
                else:
                    ctx.rgb(0,
                            1 - (char_key * len(drop['chars']) / 100),
                            0)

                # Pos
                y_pos = drop['y'] - char_key * self.space_between_chars
                ctx.move_to(drop['x'],
                            y_pos
                            )
                ctx.save()
                ctx.scale(1, 1)
                ctx.text(char)
                ctx.restore()

                # Not last char?
                if char_key < len(drop['chars']) - 1:
                    continue

                # Out of range?
                if y_pos > self.y_range:
                    # Create a new one
                    self.defineDrop(drop['x'])
                    # Remove the current drop
                    self.rain.pop(drop_key)

    def defineDrop(self, x: int):
        # Initial position
        drop = {
            'x': x,
            'y': random.randint(self.initial_y[0], self.initial_y[1]),
        }
        # Characters
        drop['chars'] = []
        for i in range(self.length):
            drop['chars'].append(random.choice(self.charset))

        self.rain.append(drop)


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run
    st3m.run.run_view(Matrix(ApplicationContext()))
